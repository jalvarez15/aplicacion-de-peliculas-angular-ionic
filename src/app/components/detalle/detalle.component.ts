import { Component, Input, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { PeliculaDetalle, Cast } from '../../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {

  @Input() id;
  pelicula: PeliculaDetalle = {};
  actores: Cast[] = [];
  oculto = 150;
  favoritos = 'star-outline';

  optSlidersActores = {
    slidesPerView: 3.3,
    freeMode: true,
    spaceBetween: -5
  }

  constructor(
    private moviesService: MoviesService,
    private modalCtlr: ModalController,
    private dataLocal: DataLocalService) { }

  async ngOnInit() {
    const existe = await this.dataLocal.existePeliculas(this.id);

    (existe) ? this.favoritos = 'star' : this.favoritos = 'star-outline';

    this.moviesService.getPeliculaDetalle(this.id).subscribe( resp => {
      console.log(resp);
      this.pelicula = resp;
    });
    this.moviesService.getActoresPelicula(this.id).subscribe( resp => {
      console.log(resp);
      this.actores = resp.cast;
    });
  }

  regresar(){
    this.modalCtlr.dismiss();
  }

  favorito(){
    const existe = this.dataLocal.guardarPelicula(this.pelicula);
    this.favoritos = (existe)? 'star' : 'star-outline';
  }

}
