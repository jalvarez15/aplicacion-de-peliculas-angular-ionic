import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'existeImagen'
})
export class ExisteImagenPipe implements PipeTransform {

  transform(peliculas: any[]): any[] {
    return peliculas.filter( peli => {
      return peli.backdrop_path;
    });
  }

}
