import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagenPipe } from './imagen.pipe';
import { ParesPipe } from './pares.pipe';
import { ExisteImagenPipe } from './existe-imagen.pipe';



@NgModule({
  declarations: [
    ImagenPipe,
    ParesPipe,
    ExisteImagenPipe
  ],
  exports: [
    ImagenPipe,
    ParesPipe,
    ExisteImagenPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
